---
title: '`r paste0("Structure of **", basename(path), "** folder")`'
subtitle: '`r paste0("from *", path, "*")`' 
author: '`r author`'
date: '`r Sys.time()`'
always_allow_html: true
fontsize: 12pt
link-citations: yes
linkcolor: blue
output:
  html_document:
    fig_caption: yes
    highlight: tango
    number_section: false
    theme: cosmo
    toc: yes
    toc_float: yes
    keep_md: no
    df_print: paged
---


```{css css-setup, echo=FALSE}
ul {list-style-type: disc;}
```

```{r r-setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
load_packs <- c("DT")
sapply(load_packs, library, character.only = T)

wrap_dt <- function(dt, ...){
   para <- as.list(formals(DT::datatable))

   para$class <- "display compact"
   para$rownames = FALSE
   para$filter = "bottom"
   para$autoHideNavigation = FALSE
   para$escape = TRUE
   para$options <- list(scrollX = TRUE, scrollY = FALSE, autowidth = TRUE, pageLength = 20)
   input_list <- list(...)

   for (i in setdiff(names(input_list), "options")) {
      para[i] <- input_list[[i]]
   }

   for (i in names(input_list$options)) {
       para$options[i] <- input_list$options[[i]]
   }

   para$data <- dt
   do.call(what = datatable, para)
}
```

```{r working-chunk, eval = F, include = F}
path <- et
structure = e2
```

```{r folder-description, results =  "asis"}
description2print <- paste0("The <b>   ", structure[[path]]$folder_name, "</b> folder    consists on:  \n\n  > ", structure[[path]]$description)
if (!stringr::str_detect(description2print, "([.])$")) {
   description2print <- paste0(description2print, ".")
}  

if (!is.null(structure[[path]]$untracked_folders)) {
   if (length(structure[[path]]$untracked_folders) > 0) { 
      addtexte <- paste0('  \n\n <p style="font-size:10pt"> The untracked sub-folders are: ',
                      knitr::combine_words(basename(structure[[path]]$untracked_folders)), '</p>')
      description2print <- paste(description2print, addtexte)
   }
}

if (!is.null(structure[[path]]$readme)) {
   unique_readme <- unique(dirname(structure[[path]]$readme))
   
   for (l in 1:length(unique_readme)) {
      thedirname <- unique_readme[l]
      thereadme <- structure[[path]]$readme[dirname(structure[[path]]$readme) == thedirname]
      
      old_texte <- basename(thedirname)
      thelink <- file.path("file:", thereadme)
         if (length(thereadme) == 1) {
           new_text <- paste0("[", old_texte, "](", thelink, ")")
           new_text <- stringr::str_replace(new_text, paste0("/",path), ".")
         } else {
           new_text <- paste0(old_texte, " (",paste0("[", tools::file_ext(thelink),"](", thelink, ")", collapse = ", "), ")")
         }
         description2print <- stringr::str_replace(description2print, pattern = old_texte, replacement = new_text)
   }
}

description2print <- stringr::str_replace_all(description2print, "([.]){2,}", ".")
description2print <- stringr::str_replace_all(description2print, "( [.])", ".")
description2print <- stringr::str_replace_all(description2print, "([.] ?){2,}", ".")

cat(description2print)
```

# Description of sub-folders

```{r details-hyperlink, results = "asis"}
intro_desc <- "The description of the sub-folders, and their potential untracked sub-folders follows:"
all_readme <- unlist(lapply(structure, function(k) k$readme))
if (!is.null(all_readme)) {
   intro_desc <- stringr::str_replace(intro_desc, ":", " (blue texts are hyperlinks to similar readme files):")
}
cat(intro_desc)
```

```{r print-subfolders, results = "asis"}
#devtools::load_all()
toprint <- print_structure_info(path = path, structure = structure)
```

# Quantitative summary 

A tabular summary of quantitative information by subfolder follows.


```{r tabular-sum, results = "hide", eval = T}
format_output <- knitr::pandoc_to()
if (is.null(format_output)) format_output <- "html"
tab_col <- c("Folder name" = "folder_name",
             "N of sub-folders (next level)" = "n_folders_child", 
             "N of files (next level)" = "n_files_child", 
             "N of sub-folders (all levels)" = "n_folders_all",
             "N of files (all levels)" = "n_files_all", 
             "Hidden folders" = "hidden_folders",
             "Last modification" = "last_modification", 
             "Size" = "size",
             "N of untracked folders" = "n_untracked_folders")

print_tab <- data.table::rbindlist(lapply(structure, function(k) k[tab_col]), use.names = T, fill = T)

nstructure <- stringr::str_count(names(structure), .Platform$file.sep)
nstructure <- nstructure - min(nstructure)
add_space <- sapply(nstructure, function(k) paste0(paste0(rep("-", k), collapse = ""), " "))

print_tab[, `folder_name` := paste0(add_space, `folder_name`)]

if (format_output == "html") {
  print_tab[, folder_name := paste0( "<b>", folder_name, "</b>")]
}

multiple_choice <- c(octet = 0, ko = 3, Mo = 6, Go = 9, To = 12, Po = 15, Eo = 18, Zo = 21, Yo = 24)

n_digit <- nchar(print_tab$size)
choose_multiple <- sapply(n_digit, function(i) multiple_choice[which.max(which(i - multiple_choice > -1))])
print_tab[, `Size %` := round(100 * size / max(size, na.rm = T))]
print_tab[, size := paste(round(size / 10^choose_multiple, 1), names(choose_multiple))]

colnames(print_tab)[colnames(print_tab) %in% tab_col] <- names(tab_col)

print_cap <- paste0("Folder informations: number of ticks before the folder name is the subfolder level. N is for number. The hidden folders and files were counted if `hidden` is true. Size is rounding of the sum for the files in the folder and size % is the relative size of the subfolder compare to the main folder.")

check_hidden <- unique(print_tab$`Hidden folders`)
if (length(check_hidden) == 1) {
   print_tab <- print_tab[, !c("Hidden folders")]
   print_cap <- stringr::str_replace(print_cap, "The hidden folders and files were counted if `hidden` is true.", paste("The hidden folders and files were", ifelse(isTRUE(check_hidden), "", "not"), "counted."))
}
```

```{r tabular-sum-html, results = "asis", eval = ifelse(format_output == "html", T, F)}
wrap_dt(print_tab, caption =  print_cap, escape = F )
```

```{r tabular-sum-pdf, results = "asis", eval = ifelse(format_output == "latex", T, F)}
# not available because hyperlink issues with pdf
library(kableExtra)
print(xtable::xtable(print_tab, caption = print_cap), include.rownames = F)
```
